$(function () {
    var time = '';
    
    async function getMessages() {
        try {
            var messages = await $.get('http://146.185.154.90:8000/messages');
            messages.map(function (message) {
                $('#list').append(`<li>
                <span class="autor">` + message.author + `</span>
                <span class="date">` + moment(message.datetime).format('DD MMM YYYY, HH:mm') +`</span>
                <p class="text">` + message.message + `</p>
            </li>`);
            });
            time = messages[messages.length - 1].datetime;
        } catch (error) {
            alert('Error: ' + error.status);
        }
    }
    
    setInterval(async function () {
        try {
            var lastMessage = await $.get('http://146.185.154.90:8000/messages?datetime=' + time);
            if (lastMessage.length !== 0) {
                lastMessage.map(function (message) {
                    $('#list').append(`<li>
                        <span class="autor">` + message.author + `</span>
                        <span class="date">` + moment(message.datetime).format('DD MMM YYYY, HH:mm') +`</span>
                        <p class="text">` + message.message + `</p>
                    </li>`);
                    time = message.datetime;
                });
            }
        } catch (error) {
            alert('Error: ' + error.status);
        }
    }, 2000);
    
    getMessages();
    
    $('#send').on('click', async function (e) {
        e.preventDefault();
        var message = $('#message').val();
        var author = $('#name').val();
        try {
            var request = await $.ajax({
                method: 'POST',
                url: 'http://146.185.154.90:8000/messages',
                data: {
                    message: message,
                    author: author
                }
            });
            $('.send-block form textarea').val('');
        } catch (error) {
            alert('Error: ' + error.status);
        }
    });
});